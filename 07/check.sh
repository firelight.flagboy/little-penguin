#!/bin/bash
DEBUG_FS_ROOT=/sys/kernel/debug

set -x

function fail() {
  echo -e "\e[31mERROR\e[0m: $@" >&2
  exit 1
}

function check_perm() {
  local perm=$(stat -c '%A' $1)
  case $perm in
  ($2) ;;
  (*) fail "invalide perm, got $perm expected $2 on $1";;
  esac
}

function test_device() {
  local DEVICE_PATH=$DEBUG_FS_ROOT/fortytwo/id
  local TMP_FILE=$(mktemp)

  trap "rm $TMP_FILE" EXIT INT
  test "$(cat $DEVICE_PATH)" == "fbenneto" || fail "output for $DEVICE_PATH should be 'fbenneto'"

  echo foo > $DEVICE_PATH                        && fail "foo isn't fbenneto"
  echo fbenneto > $DEVICE_PATH                   && fail "'fbenneto\\n' isn't fbenneto"
  echo -n abcdefgh > $DEVICE_PATH                && fail "login is not correct"
  echo -n fbenneto > $DEVICE_PATH                || fail "login should be correct"

  cat $DEVICE_PATH $DEVICE_PATH > $TMP_FILE
  cat $TMP_FILE > $DEVICE_PATH || fail "chaining login should work"

  (cat $TMP_FILE; echo -n "abcdefgh") > $DEVICE_PATH && fail "chaining chaining with garbage at the end"
}

function test_folder() {
  [ -d $DEBUG_FS_ROOT/fortytwo ] || fail "folder fortytwo should exist"
  check_perm $DEBUG_FS_ROOT/fortytwo 'dr?xr-xr-x'
}

function test_id() {
  local ID_PATH=$DEBUG_FS_ROOT/fortytwo/id

  [ -f $ID_PATH ] || fail "missing file fortytwo/id"
  check_perm $ID_PATH '-rw-rw-rw-'
  test_device
}

function test_jiffies() {
  local JIFFIES_PATH=$DEBUG_FS_ROOT/fortytwo/jiffies

  [ -f $JIFFIES_PATH ] || fail "missing file fortytwo/jiffies"
  check_perm $JIFFIES_PATH '-r--r--r--'
  grep -q '^[1-9][0-9]*$' $JIFFIES_PATH || fail "fortytwo/jiffies should contain only number"
}

function test_foo() {
  local FOO_PATH=$DEBUG_FS_ROOT/fortytwo/foo

  [ -f $FOO_PATH ] || fail "missing file fortytwo/foo"
  check_perm $FOO_PATH '-rw-r--r--'
  echo "hello world" > $FOO_PATH
  [ "$(cat $FOO_PATH)" == "hello world" ] || fail "error in read/write"
  [ "$(cat -e $FOO_PATH)" == "hello world$" ] || fail "error in read/write"
}

function test_foo_hard() {
  local FOO_PATH=$DEBUG_FS_ROOT/fortytwo/foo

  set +x
  set -e
  for loop in {0..100}; do
    (
      local SIZE=$(($RANDOM % 10000))
      printf "%03d writing %5d bytes\n" $loop $SIZE
      head -n $SIZE /dev/random > $FOO_PATH
      printf "%03d done writing %5d bytes\n" $loop $SIZE
    ) &
    (
      printf "%03d reading\n" $loop
      cat $FOO_PATH > /dev/null
      printf "%03d done reading\n" $loop
    ) &
  done
  set -x
  wait
}

./build.sh || fail "fail to build"

insmod fortytwo-c.ko || fail "fail to load module"

test_folder
test_id
test_jiffies
test_foo
# test_foo_hard

rmmod fortytwo-c.ko || fail "fail to remove module"

[ ! -d $DEBUG_FS_ROOT/fortytwo ] || fail "folder fortytwo should be deleted"
