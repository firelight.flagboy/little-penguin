#include <linux/debugfs.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/jiffies.h>
#include <linux/kernel.h>
#include <linux/miscdevice.h>
#include <linux/module.h>
#include <linux/mutex.h>
#include <linux/stat.h>
#include <linux/string.h>

#define LOGIN "fbenneto"
#define LOGIN_SIZE (sizeof(LOGIN) - 1)
#define MODULE_NAME "fortytwo-c"
#define LOG_PREFIX MODULE_NAME ": "

#define ID_LOG_PREFIX LOG_PREFIX "id: "

static ssize_t id_read(struct file *file, char *user, size_t size, loff_t *offs)
{
	pr_info(ID_LOG_PREFIX
		"reading from device (offset: %zu, to_write: %zu)\n",
		*offs, size);
	return simple_read_from_buffer(user, size, offs, LOGIN, LOGIN_SIZE);
}

static ssize_t id_write(struct file *file, const char *user, size_t size,
			loff_t *offs)
{
	ssize_t res;
	char buffer[LOGIN_SIZE] = { 0 };
	loff_t offset = 0;

	pr_info(ID_LOG_PREFIX "writing to device (offset: %zu, to_read: %zu)\n",
		*offs, size);
	res = simple_write_to_buffer(buffer, sizeof(buffer), &offset, user,
				     size);
	pr_info(ID_LOG_PREFIX "bytes read: (char[%zu])\"%.*s\"\n", res, res,
		buffer);
	if (res == LOGIN_SIZE) {
		if (memcmp(LOGIN, buffer, LOGIN_SIZE) == 0) {
			return LOGIN_SIZE;
		}
	}
	return -EINVAL;
}

static struct file_operations id_ops = {
	.owner = THIS_MODULE,
	.read = &id_read,
	.write = &id_write,
};

#define JIFFIES_LOG_PREFIX LOG_PREFIX "jiffies: "
static ssize_t jiffies_read(struct file *file, char *user, size_t size,
			    loff_t *offs)
{
	char buffer[128];
	int res;
	u64 jiffies_time = get_jiffies_64();

	pr_info(JIFFIES_LOG_PREFIX
		"reading from device (offset: %zu, to_write: %zu)\n",
		*offs, size);
	pr_info(JIFFIES_LOG_PREFIX "jiffies time: %llu\n", jiffies_time);

	res = snprintf(buffer, sizeof(buffer), "%llu", jiffies_time);
	if (res < 0)
		return res;
	return simple_read_from_buffer(user, size, offs, buffer, res);
}

static struct file_operations jiffies_ops = {
	.owner = THIS_MODULE,
	.read = &jiffies_read,
};

#define FOO_LOG_PREFIX LOG_PREFIX "foo: "

DEFINE_MUTEX(foo_mutex);

static struct buffer {
	char buffer[PAGE_SIZE];
	size_t len;
} foo_buffer;

static bool file_is_blocking(struct file *file)
{
	return (file->f_flags & O_NONBLOCK) == 0;
}

static int file_get_lock(struct file *file)
{
	pr_info(FOO_LOG_PREFIX "try to acquire lock\n");
	if (file_is_blocking(file)) {
		mutex_lock(&foo_mutex);
	} else if (mutex_trylock(&foo_mutex) == 0) {
		pr_info(FOO_LOG_PREFIX "lock would have blocked\n");
		return -EAGAIN;
	}
	pr_info(FOO_LOG_PREFIX "lock acquired\n");
	return 0;
}

static void file_unlock()
{
	mutex_unlock(&foo_mutex);
	pr_info(FOO_LOG_PREFIX "lock unlocked\n");
}

static ssize_t foo_read(struct file *file, char *user, size_t size,
			loff_t *offs)
{
	ssize_t res;

	res = file_get_lock(file);
	if (res < 0) {
		return res;
	}
	pr_info(FOO_LOG_PREFIX
		"reading from device (offset: %zu, to_write: %zu)\n",
		*offs, size);
	res = simple_read_from_buffer(user, size, offs, foo_buffer.buffer,
				      foo_buffer.len);
	file_unlock();
	return res;
}

static ssize_t foo_write(struct file *file, const char *user, size_t size,
			 loff_t *offs)
{
	ssize_t res;
	loff_t offset = 0;

	res = file_get_lock(file);
	if (res < 0) {
		return res;
	}
	pr_info(FOO_LOG_PREFIX
		"writing to device (offset: %zu, to_read: %zu)\n",
		*offs, size);
	res = simple_write_to_buffer(foo_buffer.buffer,
				     sizeof_field(struct buffer, buffer),
				     &offset, user, size);
	pr_info(FOO_LOG_PREFIX "bytes read: %zd\n", res);
	if (res < 0)
		goto exit_foo_write;
	foo_buffer.len = res;
exit_foo_write:
	file_unlock();
	return res;
}

static struct file_operations foo_ops = {
	.owner = THIS_MODULE,
	.read = &foo_read,
	.write = &foo_write,
};

struct dentry *folder;
struct dentry *id_file;
struct dentry *jiffies_file;
struct dentry *foo_file;

static int __init fortytwo_init(void)
{
	long error = 0;

	pr_info(LOG_PREFIX "Loading module\n");
	folder = debugfs_create_dir("fortytwo", NULL);
	if (IS_ERR(folder)) {
		error = PTR_ERR(folder);
		goto goout;
	}

	pr_info(LOG_PREFIX "creating file 'id'\n");
	id_file = debugfs_create_file("id", S_IRUGO | S_IWUGO, folder, NULL,
				      &id_ops);
	if (IS_ERR(id_file)) {
		error = PTR_ERR(id_file);
		goto goout;
	}

	pr_info(LOG_PREFIX "creating file 'jiffies'\n");
	jiffies_file = debugfs_create_file("jiffies", S_IRUGO, folder, NULL,
					   &jiffies_ops);
	if (IS_ERR(jiffies_file)) {
		error = PTR_ERR(jiffies_file);
		goto goout;
	}

	pr_info(LOG_PREFIX "creating file 'foo'\n");
	foo_file = debugfs_create_file("foo", S_IWUSR | S_IRUGO, folder, NULL,
				       &foo_ops);
	if (IS_ERR(foo_file))
		error = PTR_ERR(foo_file);
	mutex_init(&foo_mutex);
goout:
	return error;
}

static void __exit fortytwo_exit(void)
{
	pr_info(LOG_PREFIX "Cleaning up module\n");
	debugfs_remove(folder);
	mutex_destroy(&foo_mutex);
}

module_init(fortytwo_init);
module_exit(fortytwo_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("florian bennetot");
MODULE_DESCRIPTION("debugfs test");
