#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("florian bennetot");
MODULE_DESCRIPTION("module that say hello");

static int __init hello_world_init(void)
{
	printk("Hello World !\n");
	return 0;
}

static void __exit hello_world_exit(void)
{
	printk("Cleaning up module.\n");
	return;
}

module_init(hello_world_init);
module_exit(hello_world_exit);
