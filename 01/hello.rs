#![no_std]
#![feature(allocator_api, global_asm)]

use kernel::{prelude::*, print_macro};

module! {
    type: HelloWorld,
    name: b"hello_world",
    author: b"florian bennetot",
    description: b"Simple Rust kernel module that say hello",
    license: b"GPL v2",
    params: {
    },
}

struct HelloWorld {}

impl KernelModule for HelloWorld {
    fn init() -> KernelResult<Self> {
        print_macro!(INFO, true, "Hello World !\n");
        Ok(Self {})
    }
}

impl Drop for HelloWorld {
    fn drop(&mut self) {
        print_macro!(INFO, true, "Cleaning up module.\n");
    }
}
