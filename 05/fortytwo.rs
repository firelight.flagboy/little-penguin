#![no_std]
#![feature(allocator_api, global_asm)]

use kernel::{declare_file_operations, file_operations::FileOperations, prelude::*};

module_misc_device! {
    type: FortyTwo,
    name: b"fortytwo",
    author: b"florian bennetot",
    description: b"misc char module that write/check dev student login",
    license: b"GPL v2",
}

#[derive(Default)]
struct FortyTwo {}

impl FortyTwo {
    const LOGIN: &'static str = "fbenneto";
    const LOGIN_SIZE: usize = FortyTwo::LOGIN.len();
}

impl FileOperations for FortyTwo {
    declare_file_operations!(read, write);

    fn read(
        &self,
        _file: &kernel::file_operations::File,
        data: &mut kernel::user_ptr::UserSlicePtrWriter,
        offset: u64,
    ) -> KernelResult<usize> {
        pr_info!(
            "reading from device (offset: {}, to_write: {})\n",
            offset,
            data.len()
        );
        if offset == 0 {
            data.write_slice(FortyTwo::LOGIN.as_bytes())?;
            Ok(FortyTwo::LOGIN.len())
        } else {
            Ok(0)
        }
    }

    fn write(
        &self,
        data: &mut kernel::user_ptr::UserSlicePtrReader,
        offset: u64,
    ) -> KernelResult<usize> {
        let mut buffer = [0_u8; FortyTwo::LOGIN_SIZE];

        pr_info!(
            "writing to device (offset: {}, to_read: {})\n",
            offset,
            data.len()
        );
        if data.len() >= buffer.len() {
            data.read_slice(&mut buffer)?;
            if buffer == FortyTwo::LOGIN.as_bytes() {
                return Ok(buffer.len());
            }
        }
        Err(kernel::Error::EINVAL)
    }
}
