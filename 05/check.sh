set -x

function fail() {
  echo "$@" >&2
  exit 1
}

function test_device() {
  test "$(cat /dev/fortytwo)" == "fbenneto" || fail "output for /dev/fortytwo should be 'fbenneto'"

  echo foo > /dev/fortytwo                        && fail "foo isn't fbenneto"
  echo fbenneto > /dev/fortytwo                   && fail "'fbenneto\\n' isn't fbenneto"
  echo -n abcdefgh > /dev/fortytwo                && fail "login is not correct"
  echo -n fbenneto > /dev/fortytwo                || fail "login should be correct"
  cat /dev/fortytwo /dev/fortytwo > /dev/fortytwo || fail "chaining login should work"
  (cat /dev/fortytwo /dev/fortytwo; echo -n "abcdefgh") > /dev/fortytwo && fail "chaining chaining with garbage at the end"
}

function test_module() {
  echo "TESTING: $1"
  insmod $1
  test_device
  rmmod $1
  echo "$1 is Valid"
}

./build.sh

if lsmod | grep -q fortytwo; then
  rmmod fortytwo
fi

test_module fortytwo.ko
test_module fortytwo-c.ko
