#include <linux/fs.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/miscdevice.h>
#include <linux/module.h>
#include <linux/stat.h>
#include <linux/string.h>

#define LOGIN "fbenneto"
#define LOGIN_SIZE (sizeof(LOGIN) - 1)
#define MODULE_NAME "fortytwo-c"
#define LOG_PREFIX MODULE_NAME ": "

MODULE_LICENSE("GPL");
MODULE_AUTHOR("florian bennetot");
MODULE_DESCRIPTION("misc char module that write/check dev student login");

static ssize_t fortytwo_read(struct file *file, char *user, size_t size,
			     loff_t *offs)
{
	pr_info(LOG_PREFIX "reading from device (offset: %zu, to_write: %zu)\n",
		*offs, size);
	return simple_read_from_buffer(user, size, offs, LOGIN, LOGIN_SIZE);
}

static ssize_t fortytwo_write(struct file *file, const char *user, size_t size,
			      loff_t *offs)
{
	ssize_t res;
	char buffer[LOGIN_SIZE] = { 0 };
	loff_t offset = 0;

	pr_info(LOG_PREFIX "writing to device (offset: %zu, to_read: %zu)\n",
		*offs, size);
	res = simple_write_to_buffer(buffer, sizeof(buffer), &offset, user,
				     size);
	pr_info(LOG_PREFIX "bytes read: (char[%zu])\"%.*s\"\n", res, res,
		buffer);
	if (res == LOGIN_SIZE) {
		if (memcmp(LOGIN, buffer, LOGIN_SIZE) == 0) {
			return LOGIN_SIZE;
		}
	}
	return -EINVAL;
}

static struct file_operations fortytwo_ops = {
	.owner = THIS_MODULE,
	.read = &fortytwo_read,
	.write = &fortytwo_write,
};

static struct miscdevice fortytwo_device = {
	.minor = MISC_DYNAMIC_MINOR,
	.name = "fortytwo",
	.fops = &fortytwo_ops,
	.mode = S_IRUGO | S_IWUGO,
};

static int __init fortytwo_init(void)
{
	printk(LOG_PREFIX "Loading module\n");
	return misc_register(&fortytwo_device);
}

static void __exit fortytwo_exit(void)
{
	printk(LOG_PREFIX "Cleaning up module\n");
	misc_deregister(&fortytwo_device);
}

module_init(fortytwo_init);
module_exit(fortytwo_exit);
