# little-penguin

Linux Kernel Development

- [little-penguin](#little-penguin)
  - [Assignment 00 : Build kernel from source](#assignment-00--build-kernel-from-source)
  - [Assignment 01 : Hello world module](#assignment-01--hello-world-module)
  - [Assignment 02 : Custom kernel version](#assignment-02--custom-kernel-version)
  - [Assignment 03 : Manual lint C code](#assignment-03--manual-lint-c-code)
  - [Assignment 04 : Hello world on USB keyboard plugged in](#assignment-04--hello-world-on-usb-keyboard-plugged-in)
  - [Assignment 05 : First Misc Char Driver](#assignment-05--first-misc-char-driver)
  - [Assignment 06 : Build kernel next from source](#assignment-06--build-kernel-next-from-source)
    - [Installing rust](#installing-rust)
    - [Building the kernel](#building-the-kernel)
  - [Assignment 07 : module create debugfs](#assignment-07--module-create-debugfs)
  - [Assignment 08 : Fix C code](#assignment-08--fix-c-code)
  - [Assignment 09 : module list mount point](#assignment-09--module-list-mount-point)
  - [Source](#source)

## Assignment 00 : Build kernel from source

DISCLAIMER: as the date of the build, the last linux kernel is `5.13-rc7`

first we need to get the source for linux kernel.

We git clone at the following URL [git.kernel.org/torvalds/linux.git](git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git)

```shell
git clone --depth 1 --branch v5.13-rc7
```

We use the previous config from linux-5.10.17

```shell
make mrproper # to clean the directory
cp /boot/config-linux-5.10.17[...] .config
make oldconfig # at this step we need to config the new feature
```

before building the kernel we ensure `CONFIG_LOCALVERSION_AUTO` from the configuration is set to `y`

```shell
make -j 4
```

after the build we copy the kernel file

```shell
cp -iv arch/x86/boot/bzImage /boot/vmlinuz-5.13-rc7
cp -iv System.map /boot/System.map-5.13-rc7
cp -iv .config /boot/config-5.13-rc7
```

then we edit the grub config to add entry for `linux-5.13-rc7`

```grub
menuentry "GNU/Linux, Linux 5.13-rc7" {
        insmod ext2
        set root=(hd0,1)
        linux /vmlinuz-5.13-rc7 root=/dev/sda2 ro
}
```

## Assignment 01 : Hello world module

after reading the documentations:

- [quick start](https://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git/tree/Documentation/rust/quick-start.rst)
- [coding](https://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git/tree/Documentation/rust/coding.rst)
- [docs](https://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git/tree/Documentation/rust/docs.rst)

writing the module was easy thanks to the sample code in kernel source

## Assignment 02 : Custom kernel version

We take what we do on [Assignment 00](#assignment-00--build-kernel-from-source) and add `-thor_kernel` to `EXTRAVERSION` in the `Makefile`

we edit the `EXTRAVERSION`

```shell
sed -e 's/EXTRAVERSION = \(-[a-zA-Z0-9]*\)/EXTRAVERSION = \1-thor_kernel/' Makefile -i.orig
```

we rebuild

```shell
make -j 4
```

we copy the kernel files

```shell
cp -iv arch/x86/boot/bzImage /boot/vmlinuz-5.13-rc7-thor_kernel
cp -iv .config /boot/config-5.13-rc7-thor_kernel
cp -iv System.map /boot/System.map-5.13-rc7-thor_kernel
```

we setup grub with a new entry

```grub
menuentry "GNU/Linux, Linux 5.13-rc7-thor_kernel" {
        insmod ext2
        set root=(hd0,1)
        linux /vmlinuz-5.13-rc7-thor_kernel root=/dev/sda2 ro
}
```

to disable `+` or `-dirty` in version

```diff
--- /boot/config-5.13-rc7       2021-06-25 09:31:19.000000000 +0200
+++ .config   2021-06-25 10:28:02.000000000 +0200
 CONFIG_INIT_ENV_ARG_LIMIT=32
 # CONFIG_COMPILE_TEST is not set
 CONFIG_LOCALVERSION=""
-CONFIG_LOCALVERSION_AUTO=y
+# CONFIG_LOCALVERSION_AUTO is not set
 CONFIG_BUILD_SALT=""
 CONFIG_HAVE_KERNEL_GZIP=y
 CONFIG_HAVE_KERNEL_BZIP2=y
```

we generate the patch file

```shell
# Ensure that you have commited the change
git format-patch --root HEAD -o /tmp
```

## Assignment 03 : Manual lint C code

using `.clang-format` aaaand it's done

## Assignment 04 : Hello world on USB keyboard plugged in

By reading some documentation, on how to configure udev rules:

- [writing udev rules](http://www.reactivated.net/writing_udev_rules.html#syntax)
- [The Linux Device Model - Chap14](https://static.lwn.net/images/pdf/LDD3/ch14.pdf)

And by testing udev event with

```shell
udevadm monitor --udev --property
```

We can write a simple [rule](04/keyboards-hello.rules) for `udev` that check to following condition for the `udev` **event**:

1. if `action` is `add`
2. if `subsystem` is `input`
3. if `ID_INPUT_KEYBOARD` is set to anything (the variable is set only for keyboard)

After all these checks, we ask for `udev` to execute the command to load the module `hello.ko`

## Assignment 05 : First Misc Char Driver

from the doc (⚠ some of the link are from the documentation generated by `make rustdoc` & hosted by `python3 -m http.server`):

- [module_misc_device](http://192.168.0.21:8000/rust/doc/module/macro.module_misc_device.html)
- [declare_file_operations](http://192.168.0.21:8000/rust/doc/kernel/macro.declare_file_operations.html)
- [FileOperations](http://192.168.0.21:8000/rust/doc/kernel/file_operations/trait.FileOperations.html)
- [rust_miscdev](https://github.com/Rust-for-Linux/linux/blob/rust/samples/rust/rust_miscdev.rs)

The goal of this assignment is simple.
We create a module that register a misc device file name `fortytwo`, It has the following behavior:

- when we read the misc file we get the **student login** of the dev
- when we write to the misc file we get `Errno::EINVAL` if the data is not the **student login** of the dev
  > for the data, I think of data chunck. I wanted that if the data is only a repetition of the login that the check will accept the data \
  > i.e.: *loginlogin* will be accepted

## Assignment 06 : Build kernel next from source

For the kernel `linux-next` I'll use the version `20210624`.
I'll also test the feature `RUST` from [rust/quick-start.rst](https://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git/tree/Documentation/rust/quick-start.rst)
we required the following package:

- llvm toolchain (clang, ld.lld)
- rust toolchain (rustup, bindgen (crate), cargo, rustc, rustfmt, clippy)

### Installing rust

install rust toolchain from [rust-lang.org](https://www.rust-lang.org/tools/install)

```shell
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

set nightly toolchain as default

```shell
rustup default nightly-2021-02-20
```

add rust src for cross-compile `core` `alloc` crates

```shell
rustup component add rust-src
```

install [bindgen](https://crates.io/crates/bindgen), bindings to C/C++ libs

```shell
cargo install --locked --version 0.56.0 bindgen
```

### Building the kernel

We clone the source from [git.kernel.org/next/linux-next.git](git://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git)

```shell
git clone --depth 1 --branch next-20210624 git://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git
```

we clean the repo

```shell
make mrproper
```

we copy the config from linux-5.13-rc7 (a copy of .config can be found [here](06/config-next-20210624))

```shell
cp /boot/config-5.13-rc7 .config
make oldconfig
```

we compile the kernel with `LLVM=1` as suggested

```shell
make LLVM=1 -j4
```

we install the requirement to build the modules

```shell
make modules_install
```

after, we copy the file to `/boot`

```shell
cp -iv arch/x86/boot/bzImage /boot/vmlinuz-next-20210624
cp -iv .config /boot/config-next-20210624
cp -iv System.map /boot/System.map-next-20210624
```

and we add the entry to grub

```grub
menuentry "GNU/Linux, Linux next-20210624" {
        insmod ext2
        set root=(hd0,1)
        linux /vmlinuz-next-20210624 root=/dev/sda2 ro raid=noautodetect
}
```

while booting the kernel-next-20210624, I got error with raid autodetect and timeout on filesystem waiting to be available.
The workaround for me was `raid=noautodetect` (as suggested by the boot log 😳) since I don't use raid in my config

## Assignment 07 : module create debugfs

By reading the docs about the `debugfs` filesystem:

- [filesystems - debugfs](https://www.kernel.org/doc/html/latest/filesystems/debugfs.html)
- [debugfs - api](https://www.kernel.org/doc/html/latest/filesystems/api-summary.html#the-debugfs-filesystem)
  - [create file](https://www.kernel.org/doc/html/latest/filesystems/api-summary.html#c.debugfs_create_file)
  - [create dir](https://www.kernel.org/doc/html/latest/filesystems/api-summary.html#c.debugfs_create_dir)
  - [remove](https://www.kernel.org/doc/html/latest/filesystems/api-summary.html#c.debugfs_remove)

the api is alike to `miscdev` api with:

- `debugfs_create_file` / `debugfs_create_dir` => `misc_register`
- `debugfs_remove` => `misc_deregister`

## Assignment 08 : Fix C code

Thanks to `clang-format` formatting the file is fast.
But we also need to fix the code.

with the following macro in the file:

```C
MODULE_LICENSE("LICENSE");
MODULE_AUTHOR("Louis Solofrizzo <louis@ne02ptzero.me>");
MODULE_DESCRIPTION("Useless module");
```

we understand that the file is used to build a module.

with `.name` in the `struct miscdevice` indicate that the module will reverse something

```C
static struct miscdevice myfd_device = {
  .minor = MISC_DYNAMIC_MINOR,
  .name = "reverse",
  .fops = &myfd_fops,
};
```

TL,DR: The goal of this module is to create a device that reserve the data writed to it up to `PAGE_SIZE`

## Assignment 09 : module list mount point

The last assignment is a lot harder because the kernel doc miss the documentation on the `procfs` filesystem, beside the following that I found usefull:

- [seq_file interface](https://www.kernel.org/doc/html/latest/filesystems/seq_file.html)
  - [create proc entry](https://www.kernel.org/doc/html/latest/filesystems/seq_file.html?highlight=proc_create#deprecated-create-proc-entry)
  - [format output](https://www.kernel.org/doc/html/latest/filesystems/seq_file.html?highlight=proc_create#formatted-output)
  - [simple api](https://www.kernel.org/doc/html/latest/filesystems/seq_file.html?highlight=proc_create#the-extra-simple-version)
- [internal filesystem api](https://github.com/torvalds/linux/blob/master/fs/mount.h)
- [mounts implementation](https://github.com/torvalds/linux/blob/master/fs/proc_namespace.c)

## Source

- [Subject](https://cdn.intra.42.fr/pdf/pdf/13285/en.subject.pdf)
- [git.kernel.org](https://git.kernel.org/)
- [submitting pactches](https://www.kernel.org/doc/html/latest/process/submitting-patches.html)
- [rust - bindgen](https://crates.io/crates/bindgen)
- [writing udev rules](http://www.reactivated.net/writing_udev_rules.html#syntax)
- [The Linux Device Model - Chap14](https://static.lwn.net/images/pdf/LDD3/ch14.pdf)
- [filesystems - debugfs](https://www.kernel.org/doc/html/latest/filesystems/debugfs.html)
- [debugfs - api](https://www.kernel.org/doc/html/latest/filesystems/api-summary.html#the-debugfs-filesystem)
  - [create file](https://www.kernel.org/doc/html/latest/filesystems/api-summary.html#c.debugfs_create_file)
  - [create dir](https://www.kernel.org/doc/html/latest/filesystems/api-summary.html#c.debugfs_create_dir)
- [seq_file interface](https://www.kernel.org/doc/html/latest/filesystems/seq_file.html)
  - [create proc entry](https://www.kernel.org/doc/html/latest/filesystems/seq_file.html?highlight=proc_create#deprecated-create-proc-entry)
  - [format output](https://www.kernel.org/doc/html/latest/filesystems/seq_file.html?highlight=proc_create#formatted-output)
  - [simple api](https://www.kernel.org/doc/html/latest/filesystems/seq_file.html?highlight=proc_create#the-extra-simple-version)
- [internal filesystem api](https://github.com/torvalds/linux/blob/master/fs/mount.h)
- [mounts implementation](https://github.com/torvalds/linux/blob/master/fs/proc_namespace.c)
