#!/bin/bash
DEBUG_FS_ROOT=/sys/kernel/debug

set -x

function fail() {
  echo -e "\e[31mERROR\e[0m: $@" >&2
  exit 1
}

function check_perm() {
  local perm=$(stat -c '%A' $1)
  case $perm in
  ($2) ;;
  (*) fail "invalide perm, got $perm expected $2 on $1";;
  esac
}

./build.sh || fail "fail to build"

insmod mymounts.ko || fail "fail to load module"

[ -f /proc/mymounts ] || fail "/proc/mymounts file should be created"
check_perm /proc/mymounts '-r--r--r--'
diff -u <(cut -f 1-2 -d' ' /proc/mounts) <(cat /proc/mymounts) || fail "invalid output"


rmmod mymounts.ko || fail "fail to remove module"

[ ! -f /proc/mymounts ] || fail "mymounts should be deleted"
