#include </usr/src/kernel-next/fs/mount.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/mount.h>
#include <linux/nsproxy.h>
#include <linux/proc_fs.h>
#include <linux/slab.h>
#include <linux/stat.h>

#define MODULE_NAME "mymounts"
#undef pr_fmt
#define pr_fmt(fmt) MODULE_NAME ": " fmt

#define MAX_PATH_S (PATH_MAX + 1)

static int show_mount_entry(struct seq_file *m, struct mount *mnt)
{
	struct vfsmount *vmnt = &mnt->mnt;
	struct mount *rmnt = real_mount(vmnt);
	struct path mnt_path = {
		.dentry = vmnt->mnt_root,
		.mnt = vmnt,
	};
	struct super_block *sb = mnt_path.dentry->d_sb;
	int err = 0;

	if (sb->s_op->show_devname) {
		err = sb->s_op->show_devname(m, mnt_path.dentry);
		if (err)
			goto show_mount_entry_out;
	} else {
		seq_puts(m, rmnt->mnt_devname ? rmnt->mnt_devname : "none");
	}
	seq_putc(m, ' ');
	seq_path(m, &mnt_path, " \t\n\\");
	seq_putc(m, '\n');

show_mount_entry_out:
	return err;
}

static bool should_print_mount_point(struct mount *mnt)
{
	return (mnt->mnt.mnt_flags & MNT_CURSOR) == 0;
}

static int show_mounts(struct seq_file *m, void *data)
{
	struct task_struct *task = current;
	struct nsproxy *proxy = task->nsproxy;
	struct mnt_namespace *mnt_ns = proxy->mnt_ns;
	struct mount *mnt, *head;
	int err = 0;

	head = list_first_entry(&mnt_ns->list, struct mount, mnt_list);
	head->mnt.mnt_flags |= MNT_CURSOR;

	list_for_each_entry (mnt, &mnt_ns->list, mnt_list) {
		if (should_print_mount_point(mnt)) {
			err = show_mount_entry(m, mnt);
			if (err)
				break;
		}
	}

	head->mnt.mnt_flags ^= MNT_CURSOR;
	return err;
}

static int mymounts_open(struct inode *inode, struct file *file)
{
	return single_open(file, &show_mounts, NULL);
}

static struct proc_dir_entry *mymounts_entry;
static struct proc_ops mymounts_ops = {
	.proc_open = &mymounts_open,
	.proc_read = &seq_read,
};

static int __init mymount_init(void)
{
	int error = 0;

	pr_info("loading\n");
	mymounts_entry = proc_create("mymounts", S_IRUGO, NULL, &mymounts_ops);
	if (IS_ERR(mymounts_entry))
		error = PTR_ERR(mymounts_entry);
	return error;
}

static void __exit mymount_exit(void)
{
	pr_info("Cleaning up module\n");
	proc_remove(mymounts_entry);
}

module_init(mymount_init);
module_exit(mymount_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("florian bennetot");
MODULE_DESCRIPTION("list mount point");
